import * as consoleOas from "@latency.gg/lgg-console-oas";
import assert from "assert";
import * as application from "../application/index.js";

export function createCreateClientOperation(
    context: application.Context,
): consoleOas.CreateClientOperationHandler<application.Authorization> {
    return async function (incomingRequest, authorization) {
        const { user } = authorization.accessToken;

        const entity = await incomingRequest.entity();

        const createCientResult = await context.services.authApi.createClient({
            parameters: {},
            entity() {
                return {
                    name: entity.name,
                };
            },
        });
        assert(createCientResult.status === 201, "expected 201");

        const createClientResultEntity = await createCientResult.entity();

        const client = createClientResultEntity.id;

        const createOwnershipResult = await context.services.authApi.createClientOwnership({
            parameters: {
                user,
                client,
            },
            entity() {
                return {};
            },
        });

        assert(createOwnershipResult.status === 201, "expected 201");

        const clientOwnershipEntity = await createOwnershipResult.entity();

        return {
            parameters: {},
            status: 201,
            entity() {
                return {
                    id: client,
                };
            },
        };
    };
}
