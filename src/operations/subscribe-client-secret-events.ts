import * as consoleOas from "@latency.gg/lgg-console-oas";
import assert from "assert";
import * as application from "../application/index.js";
import * as queries from "../queries/index.js";
import { getQueryState } from "../utils/index.js";

export function createSubscribeClientSecretEventsOperation(
    context: application.Context,
): consoleOas.SubscribeClientSecretEventsOperationHandler<application.Authorization> {
    return async function (incomingRequest, authorization) {
        const { user } = authorization.accessToken;
        const { client } = incomingRequest.parameters;

        const clientOwnershipState = await getQueryState(
            context.services.clientOwnershipQuery,
            [user],
            context.config.linger,
        );

        if (!queries.selectClientOwnershipExists(clientOwnershipState, client)) {
            return {
                status: 404,
                parameters: {},
            };
        }

        const subscribeClientSecretResult =
            await context.services.authApi.subscribeClientSecretEvents({
                parameters: {
                    client,
                },
            });

        assert(subscribeClientSecretResult.status === 200, "expected 200");

        return {
            status: 200,
            parameters: {},
            async * entities(signal) {
                yield* subscribeClientSecretResult.entities(signal);
            },
        };
    };
}

