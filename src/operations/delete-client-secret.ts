import * as consoleOas from "@latency.gg/lgg-console-oas";
import assert from "assert";
import * as application from "../application/index.js";
import * as queries from "../queries/index.js";
import { getQueryState } from "../utils/index.js";

export function createDeleteClientSecretOperation(
    context: application.Context,
): consoleOas.DeleteClientSecretOperationHandler<application.Authorization> {
    return async function (incomingRequest, authorization) {
        const { user } = authorization.accessToken;
        const { client, digest } = incomingRequest.parameters;

        const clientOwnershipState = await getQueryState(
            context.services.clientOwnershipQuery,
            [user],
            context.config.linger,
        );

        if (!queries.selectClientOwnershipExists(clientOwnershipState, client)) {
            return {
                status: 404,
                parameters: {},
            };
        }

        const result = await context.services.authApi.deleteClientSecret({
            parameters: {
                client,
                digest,
            },
        });
        assert(result.status === 204, "expected 204");

        return {
            parameters: {},
            status: 204,
        };
    };
}

