import * as consoleOas from "@latency.gg/lgg-console-oas";
import assert from "assert";
import * as application from "../application/index.js";
import * as queries from "../queries/index.js";
import { getQueryState } from "../utils/index.js";

export function createCreateClientSecretOperation(
    context: application.Context,
): consoleOas.CreateClientSecretOperationHandler<application.Authorization> {
    return async function (incomingRequest, authorization) {
        const { user } = authorization.accessToken;
        const { client } = incomingRequest.parameters;
        const entity = await incomingRequest.entity();

        const clientOwnershipState = await getQueryState(
            context.services.clientOwnershipQuery,
            [user],
            context.config.linger,
        );

        if (!queries.selectClientOwnershipExists(clientOwnershipState, client)) {
            return {
                status: 404,
                parameters: {},
            };
        }

        const result = await context.services.authApi.createClientSecret({
            parameters: { client },
            entity() {
                return {};
            },
        });
        assert(result.status === 201, "expected 201");

        const resultEntity = await result.entity();

        return {
            parameters: {},
            status: 201,
            entity() {
                return {
                    digest: resultEntity.digest,
                    secret: resultEntity.secret,
                };
            },
        };

    };
}
