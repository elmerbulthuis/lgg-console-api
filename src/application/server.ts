import * as consoleOas from "@latency.gg/lgg-console-oas";
import * as oas3ts from "@oas3/oas3ts-lib";
import assert from "assert";
import jwt from "jsonwebtoken";
import jwks from "jwks-rsa";
import { minute } from "msecs";
import * as operations from "../operations/index.js";
import * as queries from "../queries/index.js";
import { getQueryState } from "../utils/index.js";
import { Context } from "./context.js";

export interface Authorization {
    accessToken: {
        provider: string
        subject: string
        user: string
    }
}

export type Server = consoleOas.Server<Authorization>;

export function createServer(
    context: Context,
    onError: (error: unknown) => void,
) {
    const server = new consoleOas.Server<Authorization>({});

    server.registerMiddleware(oas3ts.createErrorMiddleware(onError));

    server.registerMiddleware(
        oas3ts.createCorsMiddleware({
            allowOrigin: "*",
            maxAge: 15 * minute,
            metadata: consoleOas.metadata,
        }),
    );

    server.registerMiddleware(
        oas3ts.createHeartbeatMiddleware(),
    );

    server.registerMiddleware(async (route, request, next) => {
        const response = await next(request);
        response.headers["cache-control"] = "no-store";
        return response;
    });

    server.registerMiddleware(
        async function (this, route, request, next) {
            const operation = route ?
                oas3ts.getOperationId(consoleOas.metadata, route.name, request.method) :
                undefined;
            const stopTimer = context.metrics.operationDuration.startTimer({ operation });
            try {
                const response = await next(request);
                stopTimer({ status: response.status });
                return response;
            }
            catch (error) {
                stopTimer();

                throw error;
            }
        },
    );

    //#region auth

    const { oauthClientId, openIdConfiguration } = context.config;

    if (openIdConfiguration != null) {
        const jwksClient = jwks({
            jwksUri: context.config.openIdConfiguration.jwks_uri,
        });

        server.registerAccessTokenAuthorization(async token => {
            const provider = "gitlab";
            try {
                assert(token);

                const decoded = jwt.decode(token, { complete: true });
                assert(decoded);

                const { header } = decoded;
                const { kid } = header;
                const key = await jwksClient.getSigningKey(kid);

                const verified = jwt.verify(
                    token,
                    key.getPublicKey(),
                    {
                        audience: oauthClientId,
                        issuer: context.config.openIdConfiguration.issuer,
                        complete: true,
                    },
                );
                assert(typeof verified === "object");

                const { payload } = verified;
                assert(typeof payload === "object");

                const { sub: subject } = payload;
                if (subject == null) return;

                //#region connection

                const connectionState = await getQueryState(
                    context.services.connectionQuery,
                    [provider],
                    context.config.linger,
                );
                const connectionEntity = queries.selectConnectionEntity(
                    connectionState,
                    subject,
                );
                let user: string;
                if (connectionEntity == null) {
                    {
                        //TODO: get name from context.config.openIdConfiguration.userinfo_endpoint;

                        const result = await context.services.authApi.createUser({
                            parameters: {},
                            entity() {
                                return {
                                    name: "?",
                                };
                            },
                        });
                        const resultEntity = await result.entity();
                        user = resultEntity.id;
                    }
                    {
                        const result = await context.services.authApi.connectUserToProviderSubject({
                            parameters: {
                                provider: "gitlab",
                                subject,
                                user,
                            },
                            entity() {
                                return {};
                            },
                        });
                        const resultEntity = await result.entity();
                    }
                }
                else {
                    user = connectionEntity.user;
                }

                //#endregion

                return { provider, subject, user };
            }
            catch (error) {
                onError(error);
                return;
            }
        });
    }

    //#endregion

    //#region client

    server.registerSubscribeClientEventsOperation(
        operations.createSubscribeClientEventsOperation(context),
    );

    server.registerCreateClientOperation(
        operations.createCreateClientOperation(context),
    );

    server.registerDeleteClientOperation(
        operations.createDeleteClientOperation(context),
    );

    server.registerUpdateClientOperation(
        operations.createUpdateClientOperation(context),
    );

    //#endregion

    //#region client secret

    server.registerSubscribeClientSecretEventsOperation(
        operations.createSubscribeClientSecretEventsOperation(context),
    );

    server.registerCreateClientSecretOperation(
        operations.createCreateClientSecretOperation(context),
    );

    server.registerDeleteClientSecretOperation(
        operations.createDeleteClientSecretOperation(context),
    );

    //#endregion

    return server;
}
