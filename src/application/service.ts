import * as authOas from "@latency.gg/lgg-auth-oas";
import { createRedirectMiddleware } from "@oas3/oas3ts-lib";
import delay from "delay";
import * as queries from "../queries/index.js";
import { Config } from "./config.js";

export interface Services {
    authApi: authOas.Client
    connectionQuery: queries.ConnectionQuery
    clientOwnershipQuery: queries.ClientOwnershipQuery
    clientQuery: queries.ClientQuery
    authorizedClientQuery: queries.AuthorizedClientQuery

    destroy: () => Promise<void>
}

export function createServices(
    config: Config,
    onError: (error: unknown) => void,
): Services {
    const authApi = new authOas.Client(
        {
            baseUrl: config.authApiEndpoint,
            httpSendReceive: config.httpSendReceive,
        },
        {
        },
    );
    authApi.registerMiddleware(
        createRedirectMiddleware(),
    );

    const connectionQuery = queries.createConnectionQueryFactory(
        config,
        { authApi },
        onError,
    );

    const clientOwnershipQuery = queries.createClientOwnershipQueryFactory(
        config,
        { authApi },
        onError,
    );

    const clientQuery = queries.createClientQueryFactory(
        config,
        { authApi },
        onError,
    );

    const authorizedClientQuery = queries.createAuthorizedClientQueryFactory(
        config,
        { clientQuery, clientOwnershipQuery },
        onError,
    );

    const destroy = async () => {
        // flushing should happen after all of the leases are released!
        // we wait for a tick to make this happen
        await delay(0);
        //

        connectionQuery.flush();
        clientOwnershipQuery.flush();
        clientQuery.flush();
        authorizedClientQuery.flush();
    };

    return {
        authApi,
        connectionQuery,
        clientOwnershipQuery,
        clientQuery,
        authorizedClientQuery,
        destroy,
    };
}
