import * as authOas from "@latency.gg/lgg-auth-oas";
import assert from "assert";
import immutable from "immutable";
import { InstanceMemoizer } from "instance-memoizer";
import * as application from "../application/index.js";
import { createQueryFactory, ErrorEvent, isErrorEvent, QuerySource } from "../utils/index.js";

//#region query

export type ConnectionQuery = InstanceMemoizer<
    Promise<ConnectionQuerySource>, [string]
>

export type ConnectionQuerySource = QuerySource<
    ConnectionState, ConnectionEventUnion
>

export function createConnectionQueryFactory(
    config: application.Config,
    services: Pick<application.Services, "authApi">,
    onError: (error: unknown) => void,
): ConnectionQuery {
    return createQueryFactory({
        initialState, reduce,
        createSource,
        calculateHash: provider => provider,
        onError,
        settle: () => true,
        retryIntervalBase: config.retryIntervalBase,
        retryIntervalCap: config.retryIntervalCap,
    });

    async function* createSource(
        signal: AbortSignal,
        provider: string,
    ) {
        const result =
            await services.authApi.subscribeConnectionEvents({
                parameters: {
                    provider,
                },
            });

        assert(result.status === 200, "expected 200");

        yield* result.entities(signal);
    }
}

//#endregion

//#region state / events

export interface ConnectionEntity {
    user: string
    created: string
}

export interface ConnectionState {
    error: boolean;
    entities: immutable.Map<string, ConnectionEntity>;
}

const initialState: ConnectionState = {
    error: false,
    entities: immutable.Map(),
};

export type ConnectionEventUnion =
    authOas.SubscribeConnectionEvents200ApplicationJsonResponseSchema;

function reduce(
    state: ConnectionState,
    event: ConnectionEventUnion | ErrorEvent,
): ConnectionState {
    if (isErrorEvent(event)) {
        return {
            ...state,
            error: true,
        };
    }

    switch (event.type) {
        case "connection-snapshot": {
            let { entities } = initialState;

            entities = entities.withMutations(entities => {
                for (
                    const {
                        subject,
                        user,
                        created,
                    } of event.payload.connections
                ) {
                    assert(!entities.has(subject), "connection already exist");

                    const entity = {
                        user,
                        created,
                    };
                    entities.set(subject, entity);
                }
            });

            return {
                error: false,
                entities,
            };
        }

        case "connection-created": {
            let { entities } = state;

            const {
                subject,
                user,
                created,
            } = event.payload.connection;

            assert(!entities.has(subject), "connection already exist");

            const entity: ConnectionEntity = {
                user,
                created,
            };
            entities = entities.set(subject, entity);

            return {
                ...state,
                entities,
            };
        }

        case "connection-deleted": {
            let { entities } = state;

            const {
                subject,
            } = event.payload.connection;

            assert(entities.has(subject), "connection does not exist");
            entities = entities.delete(subject);

            return {
                ...state,
                entities,
            };
        }

        default: return state;
    }
}

//#endregion

//#region selectors

export function selectConnectionExists(
    state: ConnectionState,
    subject: string,
): boolean {
    return state.entities.has(subject);
}

export function selectConnectionEntity(
    state: ConnectionState,
    subject: string,
): ConnectionEntity | undefined {
    const connections = state.entities;
    const entity = connections.get(subject);
    if (!entity) return;

    return entity;
}

export function selectConnectionStateEqual(
    state: ConnectionState,
    otherState: ConnectionState,
) {
    return state.entities.equals(otherState.entities);
}

export function selectConnectionStateEmpty(
    state: ConnectionState,
) {
    return state.entities.isEmpty();
}

//#endregion
