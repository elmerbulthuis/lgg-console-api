export function decodeJWT(token: string) {
    const textDecoder = new TextDecoder();

    const parts = token.split(".");

    const headerBuffer = Buffer.from(parts[0], "base64url");
    const payloadBuffer = Buffer.from(parts[1], "base64url");

    const headerString = textDecoder.decode(headerBuffer);
    const payloadString = textDecoder.decode(payloadBuffer);

    const header = JSON.parse(headerString);
    const payload = JSON.parse(payloadString);

    return {
        header, payload,
    };
}

export function encodeJWT(header: object, payload: object) {
    const textEncoder = new TextEncoder();

    const headerString = JSON.stringify(header);
    const payloadString = JSON.stringify(payload);

    const headerBuffer = Buffer.from(textEncoder.encode(headerString));
    const payloadBuffer = Buffer.from(textEncoder.encode(payloadString));

    const parts = [
        headerBuffer.toString("base64url"),
        payloadBuffer.toString("base64url"),
        "",
    ];

    const token = parts.join(".");

    return token;
}

