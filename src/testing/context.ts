import * as authOas from "@latency.gg/lgg-auth-oas";
import * as consoleOas from "@latency.gg/lgg-console-oas";
import * as http from "http";
import { second } from "msecs";
import * as prom from "prom-client";
import { Promisable } from "type-fest";
import * as application from "../application/index.js";

interface Context {
    endpoints: {
        consoleApi: URL,
        authApi: URL,
    },
    servers: {
        consoleApi: application.Server,
        authApi: authOas.Server,
    },
    services: application.Services,
    createConsoleApiClient(credentials: consoleOas.ClientCredentials): consoleOas.Client,
}

export async function withContext<T>(
    onError: (error: unknown) => void,
    job: (context: Context) => Promisable<T>,
) {
    const endpoints = {
        consoleApi: new URL("http://localhost:8080"),
        authApi: new URL("http://localhost:9010"),
    };

    const abortController = new AbortController();
    const promRegistry = new prom.Registry();

    const applicationConfig: application.Config = {
        endpoint: endpoints.consoleApi,
        authApiEndpoint: endpoints.authApi,

        abortController,
        promRegistry,

        linger: 10 * second,
        retryIntervalBase: 1 * second,
        retryIntervalCap: 1 * second,
    };

    const applicationContext = application.createContext(
        applicationConfig,
        onError,
    );
    try {
        const consoleApi = application.createServer(
            applicationContext,
            onError,
        );
        const authApi = new authOas.Server({});

        const servers = {
            consoleApi,
            authApi,
        };

        const httpServers = {
            consoleApi: http.createServer(consoleApi.asRequestListener({
                onError,
            })),
            authApi: http.createServer(authApi.asRequestListener({
                onError,
            })),
        };

        const context: Context = {
            endpoints,
            servers,
            createConsoleApiClient(credentials: consoleOas.ClientCredentials) {
                return new consoleOas.Client(
                    {
                        baseUrl: endpoints.consoleApi,
                    },
                    credentials,
                );
            },
            services: applicationContext.services,
        };

        const keys = Object.keys(httpServers) as Array<keyof typeof httpServers>;
        await Promise.all(
            keys.map(async key => new Promise<void>(resolve => httpServers[key].listen(
                endpoints[key].port,
                () => resolve(),
            ))),
        );

        try {
            const result = await job(context);
            return result;
        }
        finally {
            abortController.abort();

            await Promise.all(
                keys.map(async key => new Promise<void>(
                    (resolve, reject) => httpServers[key].close(
                        error => error ?
                            reject(error) :
                            resolve(),
                    )),
                ),
            );
        }

    }
    finally {
        await applicationContext.destroy();
    }

}
